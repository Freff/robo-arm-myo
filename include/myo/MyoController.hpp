#ifndef ROBO_ARM_MYO_MYOCONTROLLER_HPP
#define ROBO_ARM_MYO_MYOCONTROLLER_HPP

#include <mutex>
#include <thread>
#include <future>

#include "../math/Coordinates.hpp"

class MyoController {

public:

    MyoController();

    math::Euler getOrientation();

    // todo: run probably needs to be private
    // todo: should be in a background_process class or something
    void run();
    void runThread();

    void stop();
    void join();

private:

    std::mutex _process_mutex;
    std::thread _work_thread;

    std::atomic<bool> _stop;

    math::Euler _orientation;
};


#endif
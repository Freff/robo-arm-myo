#ifndef ROBO_ARM_MYO_MQTTMESSAGER_HPP
#define ROBO_ARM_MYO_MQTTMESSAGER_HPP

#include <mqtt/client.h>

#include "IMessager.hpp"

class MQTTMessager : public IMessager
{
public:

    MQTTMessager() = delete;
    MQTTMessager(std::string address, std::string client_id);

    // todo declare ro5
    ~MQTTMessager();

    void publish(std::string topic, std::string message) override;

private:

    std::string _address;
    std::string _client_id;

    mqtt::client _client;
};

#endif
#ifndef ROBO_ARM_MYO_IMESSAGER_HPP
#define ROBO_ARM_MYO_IMESSAGER_HPP

#include <string>

class IMessager
{
public:
    virtual ~IMessager() = default;

    virtual void publish(std::string topic, std::string message) = 0;
};

#endif
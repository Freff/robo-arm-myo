//
// Created by chris on 20/06/18.
//

#ifndef ROBO_ARM_MYO_COORDINATES_HPP
#define ROBO_ARM_MYO_COORDINATES_HPP

// orientation scale in myolinux lib
constexpr int orientation_scale = 16384.0f;

namespace math
{
    struct Quaternion
    {
        Quaternion(double xx, double yy, double zz, double ww);

        double x;
        double y;
        double z;
        double w;
    };


    struct Euler {
        Euler();

        Euler(double r, double p, double y);

        double roll;
        double pitch;
        double yaw;
    };

    Euler toEulerAngle(const Quaternion& q);

}

#endif //ROBO_ARM_MYO_COORDINATES_HPP

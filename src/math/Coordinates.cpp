#include <cmath>

#include "../../include/math/Coordinates.hpp"

namespace math
{
    Quaternion::Quaternion(double xx, double yy, double zz, double ww)
            : x(xx / orientation_scale), y(yy / orientation_scale),
              z(zz / orientation_scale), w(ww / orientation_scale) {}


    Euler::Euler() : roll(0.0), pitch(0.0), yaw(0.0) {}

    Euler::Euler(double r, double p, double y)
            : roll(r), pitch(p), yaw(y) {}


    // https://en.wikipedia.org/wiki/Conversion_between_quaternions_and_Euler_angles
    Euler toEulerAngle(const Quaternion &q) {
        Euler euler_angle;

        // roll (x-axis rotation)
        double sinr = +2.0 * (q.w * q.x + q.y * q.z);
        double cosr = +1.0 - 2.0 * (q.x * q.x + q.y * q.y);
        euler_angle.roll = atan2(sinr, cosr);

        // pitch (y-axis rotation)
        double sinp = +2.0 * (q.w * q.y - q.z * q.x);
        if (fabs(sinp) >= 1)
            euler_angle.pitch = copysign(M_PI / 2, sinp); // use 90 degrees if out of range
        else
            euler_angle.pitch = asin(sinp);

        // yaw (z-axis rotation)
        double siny = +2.0 * (q.w * q.z + q.x * q.y);
        double cosy = +1.0 - 2.0 * (q.y * q.y + q.z * q.z);
        euler_angle.yaw = atan2(siny, cosy);

        return euler_angle;
    }

}
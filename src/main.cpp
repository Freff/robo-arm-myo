#include <chrono>
#include <thread>
#include <iostream>

#include "../include/myo/MyoController.hpp"
#include "../include/messaging/MQTTMessager.hpp"

using namespace std::literals::chrono_literals;

int main()
{
    MyoController m;

    // todo could launch this on a std::async to get myo booted while this connects
    std::unique_ptr<IMessager> _messager = std::make_unique<MQTTMessager>("ec2-18-233-230-164.compute-1.amazonaws.com", "myo");

    m.runThread();

    while (true)
    {
        auto e = m.getOrientation();

        // todo get a json library to do this
        std::string payload = "{";
        payload += "\"pitch\": " + std::to_string(e.pitch) + ",";
        payload += "\"yaw\": " + std::to_string(e.yaw) + ",";
        payload += "\"roll\": " + std::to_string(e.roll);
        payload += "}";

        _messager->publish("mychannel", payload);
        std::cout << payload << std::endl;

        std::this_thread::sleep_for(100ms);
    }
}
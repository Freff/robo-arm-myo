#include "../../include/myo/MyoController.hpp"

#include "../../libs/MyoLinux/src/myoclient.h"
#include "../../libs/MyoLinux/src/serial.h"

#include <iostream>
#include <chrono>
#include <thread>
#include <cmath>
#include <cinttypes>

using namespace myolinux;

using namespace std::literals::chrono_literals;


MyoController::MyoController()
    : _stop(false)
{

}

math::Euler MyoController::getOrientation()
{
    std::scoped_lock<std::mutex> lock (_process_mutex);
    return _orientation;
}

void MyoController::run()
{
    std::cout << "Waiting for Myo" << std::endl;

    myo::Client client(Serial{"/dev/ttyACM0", 115200});

    // Autoconnect to the first Myo device
    client.connect();
    if (!client.connected()) {
        //return 1;
    }

    std::cout << "Connected to Myo" << std::endl;

    // Print device address
    print_address(client.address());

    // Read firmware version
    auto version = client.firmwareVersion();
    std::cout << version.major << "."
              << version.minor << "."
              << version.patch << "."
              << version.hardware_rev << std::endl;


    // Read EMG and IMU
    client.setMode(myo::EmgMode::SendEmg, myo::ImuMode::SendData, myo::ClassifierMode::Disabled);

    client.onImu([this](myo::OrientationSample ori, myo::AccelerometerSample acc, myo::GyroscopeSample gyr)
                 {
                     auto q = math::Quaternion (ori[0], ori[1], ori[2], ori[3]);
                     auto e = math::toEulerAngle(q);

                     auto a = static_cast<int>((e.pitch+(float)M_PI)/(M_PI*2.0f)*270);
                     auto b = static_cast<int>((e.roll+(float)M_PI)/(M_PI*2.0f)*270);
                     auto c = static_cast<int>((e.yaw+(float)M_PI)/(M_PI*2.0f)*180);

                     b = 270 - b;

                     std::scoped_lock<std::mutex> lock (_process_mutex);
                     _orientation = math::Euler(a,b,c);

                 });

    std::cout << "entering loop" << std::endl;

    while(!_stop)
    {
        try
        {
            client.listen();
        }
        catch (const myolinux::gatt::DisconnectedException &e)
        {
            std::cout << "client not connected" << std::endl;
            client.connect();
            if (!client.connected()) {
                throw std::runtime_error("Myo could not reconnect");
            }
        }
    }

    client.disconnect();
}

void MyoController::runThread()
{
    _work_thread = std::thread([this](){run();});
}

void MyoController::stop()
{
    _stop = true;
}

void MyoController::join()
{
    _work_thread.join();
}
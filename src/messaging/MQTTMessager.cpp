#include <cstring>
#include <iostream>
#include "../../include/messaging/MQTTMessager.hpp"


MQTTMessager::MQTTMessager(std::string address, std::string client_id)
    : _address(address), _client_id(client_id), _client(_address, _client_id)
{
    std::cout << "Trying to connect to: " + address << std::endl;
    mqtt::connect_options connOpts;
    connOpts.set_keep_alive_interval(20);
    connOpts.set_clean_session(true);

    try {
        _client.connect(connOpts);
        std::cout << "Connected!" << std::endl;
    }
    catch (const mqtt::exception& exc) {
        std::cerr << "Error: " << exc.what() << " ["
                  << exc.get_reason_code() << "]" << std::endl;
    }
}

MQTTMessager::~MQTTMessager()
{
    _client.disconnect();
};

void MQTTMessager::publish(std::string topic, std::string payload)
{
    auto msg = mqtt::make_message(topic, payload);
    _client.publish(msg);
}